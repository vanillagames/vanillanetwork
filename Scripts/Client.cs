﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Networking;

public class Client : MonoBehaviour {

	[Header("[ Connection Settings ]")]
	public byte reliableChannel;
	public byte unreliableChannel;

	public int maximumConnections = 50; // Has to match server

	public int localPort = 26000; // Has to match server
	public int webPort = 26001; // Has to match server

	public string IP = "192.168.1.134";  // Has to match server

	[Header("[ Status ]")]
	public bool active;

	public int hostID = -1;
	public int connectionID = -1;

	byte errorCode;

	public NetworkError error = NetworkError.Timeout;

	[Header("[ Network Buffer ]")]
	public int bufferSize = 13; // Has to match server

	[SerializeField]
	private byte[] _sendBuffer = new byte[13];
	public byte[] sendBuffer {
		get {
			if (_sendBuffer == null) {
				_sendBuffer = new byte[bufferSize];
			}

			return _sendBuffer;
		}
		set {
			_sendBuffer = value;
		}
	}

	void OnEnable() {
		AttemptConnection();
	}

	void OnDisable() {
		DisconnectedFromServer();
	}

	public void AttemptConnection() {
		NetworkTransport.Init();

		ConnectionConfig cc = new ConnectionConfig();

		cc.AddChannel(QosType.Unreliable);
		cc.AddChannel(QosType.Reliable);

		HostTopology topology = new HostTopology(cc, maximumConnections);

		// Client

		hostID = NetworkTransport.AddHost(topology, 0);

#if UNITY_WEBGL && !UNITY_EDITOR
		connectionID = NetworkTransport.Connect(hostID, IP, webPort, 0, out errorByte);
#else
		connectionID = NetworkTransport.Connect(hostID, IP, localPort, 0, out errorCode);
#endif

		error = (NetworkError)errorCode;

		active = error == NetworkError.Ok;

		if (!active) {
			ConnectToServerFailed();
		}
	}

	public virtual void ConnectToServerFailed() {
		Debug.LogFormat("Connection to the server failed - [{0}]", error);
	}

	public virtual void ConnectToServerSucceeded() {
		Debug.LogFormat("Connection to the server succeeded - HostID [{0}] Port [{1}]", hostID, localPort);
	}

	public virtual void DisconnectedFromServer() {
		active = false;

		Debug.LogFormat("Disconnected from server.");

		NetworkTransport.Shutdown();
	}


	public virtual void Update() {
		if (!active) {
			return;
		}

		UpdateMessagePump();
		SendBuffer();
	}

	public virtual void UpdateMessagePump() {
		int recHostId; // Web or standalone?
		int connectionID; // Which connection is this coming from?
		int channelID; // Which channel?

		byte[] recBuffer = new byte[bufferSize];
		int dataSize;

		NetworkEventType eventType = NetworkTransport.Receive(out recHostId, out connectionID, out channelID, recBuffer, bufferSize, out dataSize, out errorCode);

		error = (NetworkError)errorCode;

		switch (eventType) {
			case NetworkEventType.ConnectEvent:
				ConnectToServerSucceeded();
				break;
			case NetworkEventType.DisconnectEvent:
				DisconnectedFromServer();
				break;
			case NetworkEventType.DataEvent:
				ReceiveBuffer(connectionID);
				break;
			case NetworkEventType.BroadcastEvent:

				break;
		}
	}

	public virtual void ReceiveBuffer(int connectionID) {

	}

	public virtual void SendBuffer() {
		NetworkTransport.Send(hostID, connectionID, reliableChannel, sendBuffer, bufferSize, out errorCode);
	}
}