﻿using System;

using UnityEngine;

using Vanilla.ExtensionMethods;

namespace Vanilla.Network {

	[Serializable]
	public struct ShortVector3 {
		// By what factor should the Vector3 be multiplied and divided in order to store the most important information accurately within a short?
		// This may have to change in order to match the nature of your app.
		[SerializeField]
		public short x;
		[SerializeField]
		public short y;
		[SerializeField]
		public short z;

		// Construction
		public ShortVector3(float X, float Y, float Z, float decimalFactor = 1000.0f) {
			x = (short)(X * decimalFactor);
			y = (short)(Y * decimalFactor);
			z = (short)(Z * decimalFactor);
		}

		public ShortVector3(Vector3 vector3, float decimalFactor = 1000.0f) {
			x = (short)(vector3.x * decimalFactor);
			y = (short)(vector3.y * decimalFactor);
			z = (short)(vector3.z * decimalFactor);
		}

		public override string ToString() {
			return string.Format("[{0}, {1}, {2}]", x, y, z);
		}

		public Vector3 ToVector3(float decimalFactor) {
			return new Vector3(x, y, z) / decimalFactor;
		}
	}

	/// <summary>
	/// This struct contains a compressed representation of a quaternion, aka a 'short' quaternion. The translation is handled automatically when constructed using a quaternion or Quaterion.ToShort(), and then back again via ToQuaternion().
	/// </summary>
	[Serializable]
	public struct ShortQuaternion {

		// Rotation
		[SerializeField]
		public byte rMaxIndex;

		[SerializeField]
		public short rA;

		[SerializeField]
		public short rB;

		[SerializeField]
		public short rC;

		// Construction
		public ShortQuaternion(Quaternion quaternion, float decimalFactor) {
			rMaxIndex = 0;

			float maxValue = float.MinValue;
			float sign = 1f;

			for (int i = 0; i < 4; i++) {
				float element = quaternion[i];
				float abs = Mathf.Abs(quaternion[i]);

				if (abs > maxValue) {
					sign = Mathf.Sign(element);

					rMaxIndex = (byte)i;

					maxValue = abs;
				}
			}

			if (Mathf.Approximately(maxValue, 1f)) {
				rMaxIndex += 4;

				rA = 0;
				rB = 0;
				rC = 0;
			} else {
				switch (rMaxIndex) {
					case 0:
						rA = (short)(quaternion.y * sign * decimalFactor);
						rB = (short)(quaternion.z * sign * decimalFactor);
						rC = (short)(quaternion.w * sign * decimalFactor);
						break;
					case 1:
						rA = (short)(quaternion.x * sign * decimalFactor);
						rB = (short)(quaternion.z * sign * decimalFactor);
						rC = (short)(quaternion.w * sign * decimalFactor);
						break;
					case 2:
						rA = (short)(quaternion.x * sign * decimalFactor);
						rB = (short)(quaternion.y * sign * decimalFactor);
						rC = (short)(quaternion.w * sign * decimalFactor);
						break;
					case 3:
						rA = (short)(quaternion.x * sign * decimalFactor);
						rB = (short)(quaternion.y * sign * decimalFactor);
						rC = (short)(quaternion.z * sign * decimalFactor);
						break;
					default:
						rA = 0;
						rB = 0;
						rC = 0;
						break;
				}
			}
		}

		public ShortQuaternion(float X, float Y, float Z, float W, float decimalFactor) {
			rMaxIndex = 0;

			float maxValue = float.MinValue;
			float sign = 1f;

			float abs = 0;
			float element = 0;

			for (int i = 0; i < 4; i++) {
				switch (i) {
					case 0:
						element = X;
						break;
					case 1:
						element = Y;
						break;
					case 2:
						element = Z;
						break;
					case 3:
						element = W;
						break;
				}

				abs = Mathf.Abs(element);

				if (abs > maxValue) {
					sign = Mathf.Sign(element);

					rMaxIndex = (byte)i;

					maxValue = abs;
				}
			}

			if (Mathf.Approximately(maxValue, 1f)) {
				rMaxIndex += 4;

				rA = 0;
				rB = 0;
				rC = 0;
			} else {
				switch (rMaxIndex) {
					case 0:
						rA = (short)(Y * sign * decimalFactor);
						rB = (short)(Z * sign * decimalFactor);
						rC = (short)(W * sign * decimalFactor);
						break;
					case 1:
						rA = (short)(X * sign * decimalFactor);
						rB = (short)(Z * sign * decimalFactor);
						rC = (short)(W * sign * decimalFactor);
						break;
					case 2:
						rA = (short)(X * sign * decimalFactor);
						rB = (short)(Y * sign * decimalFactor);
						rC = (short)(W * sign * decimalFactor);
						break;
					case 3:
						rA = (short)(X * sign * decimalFactor);
						rB = (short)(Y * sign * decimalFactor);
						rC = (short)(Z * sign * decimalFactor);
						break;
					default:
						rA = 0;
						rB = 0;
						rC = 0;
						break;
				}
			}
		}

		// Convert
		public Quaternion ToQuaternion(float decimalFactor) {
			if (rMaxIndex >= 4 && rMaxIndex <= 7) {
				return new Quaternion(
					(rMaxIndex == 4) ? 1f : 0f, // X is biggest
					(rMaxIndex == 5) ? 1f : 0f, // Y is biggest
					(rMaxIndex == 6) ? 1f : 0f, // Z is biggest
					(rMaxIndex == 7) ? 1f : 0f  // W is biggest
				);
			}

			float a = rA / decimalFactor;
			float b = rB / decimalFactor;
			float c = rC / decimalFactor;

			float d = Mathf.Sqrt(1f - (a * a + b * b + c * c));

			switch (rMaxIndex) {
				case 0:
					return new Quaternion(d, a, b, c);
				case 1:
					return new Quaternion(a, d, b, c);
				case 2:
					return new Quaternion(a, b, d, c);
				default:
					return new Quaternion(a, b, c, d);
			}
		}

		// Update
		public void Convert(Quaternion quaternion, float decimalFactor) {
			rMaxIndex = 0;

			float maxValue = float.MinValue;
			float sign = 1f;

			for (int i = 0; i < 4; i++) {
				float element = quaternion[i];
				float abs = Mathf.Abs(quaternion[i]);

				if (abs > maxValue) {
					sign = Mathf.Sign(element);

					rMaxIndex = (byte)i;

					maxValue = abs;
				}
			}

			if (Mathf.Approximately(maxValue, 1f)) {
				rMaxIndex += 4;

				rA = 0;
				rB = 0;
				rC = 0;
			} else {
				switch (rMaxIndex) {
					case 0:
						rA = (short)(quaternion.y * sign * decimalFactor);
						rB = (short)(quaternion.z * sign * decimalFactor);
						rC = (short)(quaternion.w * sign * decimalFactor);
						break;
					case 1:
						rA = (short)(quaternion.x * sign * decimalFactor);
						rB = (short)(quaternion.z * sign * decimalFactor);
						rC = (short)(quaternion.w * sign * decimalFactor);
						break;
					case 2:
						rA = (short)(quaternion.x * sign * decimalFactor);
						rB = (short)(quaternion.y * sign * decimalFactor);
						rC = (short)(quaternion.w * sign * decimalFactor);
						break;
					case 3:
						rA = (short)(quaternion.x * sign * decimalFactor);
						rB = (short)(quaternion.y * sign * decimalFactor);
						rC = (short)(quaternion.z * sign * decimalFactor);
						break;
					default:
						rA = 0;
						rB = 0;
						rC = 0;
						break;
				}
			}
		}
	}

	[Serializable]
	public struct Vector3Serializable {
		[SerializeField]
		public float x;
		[SerializeField]
		public float y;
		[SerializeField]
		public float z;

		// Construction
		public Vector3Serializable(float X, float Y, float Z) {
			x = X;
			y = Y;
			z = Z;
		}

		public Vector3Serializable(Vector3 vector3) {
			x = vector3.x;
			y = vector3.y;
			z = vector3.z;
		}

		public override string ToString() {
			return string.Format("[{0}, {1}, {2}]", x, y, z);
		}

		/// <summary>
		/// Automatic conversion from SerializableVector3 to Vector3
		/// </summary>
		public static implicit operator Vector3(Vector3Serializable vector3S) {
			return new Vector3(vector3S.x, vector3S.y, vector3S.z);
		}

		/// <summary>
		/// Automatic conversion from Vector3 to SerializableVector3
		/// </summary>
		public static implicit operator Vector3Serializable(Vector3 vector3) {
			return new Vector3Serializable(vector3);
		}
	}

	[Serializable]
	public struct QuaternionSerializable {
		[SerializeField]
		public float x;

		[SerializeField]
		public float y;

		[SerializeField]
		public float z;

		[SerializeField]
		public float w;

		// Construction
		public QuaternionSerializable(float X, float Y, float Z, float W) {
			x = X;
			y = Y;
			z = Z;
			w = W;
		}

		public QuaternionSerializable(Quaternion quaternion) {
			x = quaternion.x;
			y = quaternion.y;
			z = quaternion.z;
			w = quaternion.w;
		}

		public override string ToString() {
			return string.Format("[{0}, {1}, {2}, {3}]", x, y, z, w);
		}

		/// <summary>
		/// Automatic conversion from SerializableVector3 to Vector3
		/// </summary>
		public static implicit operator Quaternion(QuaternionSerializable quaternionS) {
			return new Quaternion(quaternionS.x, quaternionS.y, quaternionS.z, quaternionS.w);
		}

		/// <summary>
		/// Automatic conversion from Vector3 to SerializableVector3
		/// </summary>
		public static implicit operator QuaternionSerializable(Quaternion quaternion) {
			return new QuaternionSerializable(quaternion);
		}
	}

	[Serializable]
	public class NetworkPacketBase {

	}

	/// <summary>
	/// Uses smallest-three compression to turn rotation into a single byte and three int16.
	/// Position is compressed to three int16s in a simpler way.
	/// </summary>
	[Serializable]
	public class NetworkPose : NetworkPacketBase {
		// Position
		[SerializeField]
		public short pX;

		[SerializeField]
		public short pY;

		[SerializeField]
		public short pZ;

		// Rotation
		[SerializeField]
		public byte rMaxIndex;

		[SerializeField]
		public short rA;

		[SerializeField]
		public short rB;

		[SerializeField]
		public short rC;

		// Construction
		public NetworkPose(Vector3 position, Quaternion rotation, float decimalPrecision) {
			///-------------------------------------------------------------------------------------------------------------/
			/// Position
			///-------------------------------------------------------------------------------------------------------------/

			pX = (short)(position.x * decimalPrecision);
			pY = (short)(position.y * decimalPrecision);
			pZ = (short)(position.z * decimalPrecision);

			///-------------------------------------------------------------------------------------------------------------/
			/// Rotation
			///-------------------------------------------------------------------------------------------------------------/

			// The amazing smallest-three implementation below was originally written by StagPoint

			// https://gist.github.com/StagPoint
			// https://gist.github.com/StagPoint/bb7edf61c2e97ce54e3e4561627f6582

			// Which was based on the even more incredible network-compression insights from Glenn Fiedler at Gaffer On Games

			// https://gafferongames.com/post/snapshot_compression/

			// It was modified for use with the Our Planet project in 2019 by Lucas Hehir
			// Commentary has been left fully intact except where project-specific deviations were required.

			float maxValue = float.MinValue;
			float sign = 1f;

			// Determine the index of the largest (absolute value) element in the Quaternion.
			// We will transmit only the three smallest elements, and reconstruct the largest
			// element during decoding. 
			for (int i = 0; i < 4; i++) {
				float element = rotation[i];
				float abs = Mathf.Abs(rotation[i]);

				if (abs > maxValue) {
					// We don't need to explicitly transmit the sign bit of the omitted element because you 
					// can make the omitted element always positive by negating the entire quaternion if 
					// the omitted element is negative (in quaternion space (x,y,z,w) and (-x,-y,-z,-w) 
					// represent the same rotation.), but we need to keep track of the sign for use below.
					//sign = (element < 0) ? -1 : 1;

					sign = Mathf.Sign(element);

					// Keep track of the index of the largest element
					//maxIndex = (byte)i;

					rMaxIndex = (byte)i;

					maxValue = abs;
				}
			}

			// If the maximum value is approximately 1f (such as Quaternion.identity [0,0,0,1]), then we can 
			// reduce storage even further due to the fact that all other fields must be 0f by definition, so 
			// we only need to send the index of the largest field.
			if (Mathf.Approximately(maxValue, 1f)) {
				// Again, don't need to transmit the sign since in quaternion space (x,y,z,w) and (-x,-y,-z,-w) 
				// represent the same rotation. We only need to send the index of the single element whose value
				// is 1f in order to recreate an equivalent rotation on the receiver.

				// The + 4 is used later to identify which component of the Quaternion is the largest.
				rMaxIndex += 4;
			} else {
				// We multiply the value of each element by QUAT_PRECISION_MULT before converting to 16-bit integer 
				// in order to maintain precision. This is necessary since by definition each of the three smallest 
				// elements are less than 1.0, and the conversion to 16-bit integer would otherwise truncate everything 
				// to the right of the decimal place. This allows us to keep five decimal places.
				switch (rMaxIndex) {
					case 0:
						rA = (short)(rotation.y * sign * decimalPrecision);
						rB = (short)(rotation.z * sign * decimalPrecision);
						rC = (short)(rotation.w * sign * decimalPrecision);
						break;
					case 1:
						rA = (short)(rotation.x * sign * decimalPrecision);
						rB = (short)(rotation.z * sign * decimalPrecision);
						rC = (short)(rotation.w * sign * decimalPrecision);
						break;
					case 2:
						rA = (short)(rotation.x * sign * decimalPrecision);
						rB = (short)(rotation.y * sign * decimalPrecision);
						rC = (short)(rotation.w * sign * decimalPrecision);
						break;
					case 3:
						rA = (short)(rotation.x * sign * decimalPrecision);
						rB = (short)(rotation.y * sign * decimalPrecision);
						rC = (short)(rotation.z * sign * decimalPrecision);
						break;
				}
			}

			///-------------------------------------------------------------------------------------------------------------/
			/// Timestamp
			///-------------------------------------------------------------------------------------------------------------/

			// Timestamp implementation goes here
		}

		public Vector3 ToPosition(float decimalPrecision) {
			return new Vector3(pX / decimalPrecision, pY / decimalPrecision, pZ / decimalPrecision);
		}

		public Quaternion ToRotation(float decimalPrecision) {
			if (rMaxIndex >= 4 && rMaxIndex <= 7) {
				return new Quaternion(
					(rMaxIndex == 4) ? 1f : 0f, // X is biggest
					(rMaxIndex == 5) ? 1f : 0f, // Y is biggest
					(rMaxIndex == 6) ? 1f : 0f, // Z is biggest
					(rMaxIndex == 7) ? 1f : 0f  // W is biggest
				);
			}

			float a = rA / decimalPrecision;
			float b = rB / decimalPrecision;
			float c = rC / decimalPrecision;

			float d = Mathf.Sqrt(1f - (a * a + b * b + c * c));

			switch (rMaxIndex) {
				case 0:
					return new Quaternion(d, a, b, c);
				case 1:
					return new Quaternion(a, d, b, c);
				case 2:
					return new Quaternion(a, b, d, c);
				default:
					return new Quaternion(a, b, c, d);
			}
		}
	}
}