﻿using System;
using System.Collections.Generic;

using UnityEngine;

using Vanilla.Network;

namespace Vanilla.ExtensionMethods {
	public static class MyExtensionMethods {
		public static ShortVector3 ToShortVector3(this Vector3 vector3, float decimalFactor) {
			return new ShortVector3(vector3, decimalFactor);
		}

		public static ShortQuaternion ToShortQuaternion(this Quaternion quaternion, float decimalFactor) {
			return new ShortQuaternion(quaternion, decimalFactor);
		}
	}
}