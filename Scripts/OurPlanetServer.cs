﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vanilla.Network;

public class OurPlanetServer : Server
{
	public Transform ARAnchor;

	public GameObject remoteClientPrefab;

	private Dictionary<int, Transform> _remoteClients;
	public Dictionary<int, Transform> remoteClients {
		get {
			if (_remoteClients == null) {
				_remoteClients = new Dictionary<int, Transform>();
			}

			return _remoteClients;
		}
	}

	ShortVector3 remotePositionCache;
	ShortQuaternion remoteRotationCache;

	public override void OnEnable() {
		base.OnEnable();
	}

	public override void OnDisable() {
		base.OnDisable();
	}

	public override void Startup() {
		base.Startup();
	}

	public override void Shutdown() {
		base.Shutdown();
	}

	public override void HandleConnectEvent(int connectionID) {
		base.HandleConnectEvent(connectionID);

		if (!remoteClients.ContainsKey(connectionID)) {
			// Make this an object pool later
			GameObject newClient = Instantiate(remoteClientPrefab);

			remoteClients.Add(connectionID, newClient.transform);
		} else {
			Debug.LogWarning("The same client ConnectionID has appeared twice in seperate connection events.");
		}
	}

	public override void HandleDisconnectEvent(int connectionID) {
		base.HandleDisconnectEvent(connectionID);

		if (remoteClients.ContainsKey(connectionID)) {
			Transform outgoingClient = remoteClients[connectionID];

			remoteClients.Remove(connectionID);

			// Make this an object pool later
			Destroy(outgoingClient.gameObject);
		}
	}

	public override void Update() {
		base.Update();
	}

	public override void UpdateMessagePump() {
		base.UpdateMessagePump();
	}

	public override void ReceiveBuffer(int connectionID) {
		base.ReceiveBuffer(connectionID);

		if (remoteClients.ContainsKey(connectionID)) {
			// Deserialize the recBuffer into ShortVector3 components
			remotePositionCache.x = BitConverter.ToInt16(recBuffer, 0);
			remotePositionCache.y = BitConverter.ToInt16(recBuffer, 2);
			remotePositionCache.z = BitConverter.ToInt16(recBuffer, 4);

			// Deserialize the recBuffer into ShortQuaternion components
			remoteRotationCache.rA = BitConverter.ToInt16(recBuffer, 6);
			remoteRotationCache.rB = BitConverter.ToInt16(recBuffer, 8);
			remoteRotationCache.rC = BitConverter.ToInt16(recBuffer, 10);
			remoteRotationCache.rMaxIndex = recBuffer[12];

			// Set the remote client to the transform point of the uncompressed Vector3
			remoteClients[connectionID].position = ARAnchor.TransformPoint(remotePositionCache.ToVector3(1000.0f));

			// Set the remote client to the uncompressed Quaternion
			remoteClients[connectionID].rotation = remoteRotationCache.ToQuaternion(1000.0f);

			// With our remote client facing the right direction (?), we can use TransformDirection from the ARAnchor to establish
			// a correct networked forward direction vector
			remoteClients[connectionID].forward = ARAnchor.TransformDirection(remoteClients[connectionID].forward);
		}
	}

	public override void SendBuffer() {
		base.SendBuffer();
	}
}