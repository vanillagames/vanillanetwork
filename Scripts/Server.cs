﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Networking;

public class Server : MonoBehaviour
{
	[Header("[ Connection Settings ]")]
	public byte reliableChannel;
	public byte unreliableChannel;

	public int maximumConnections = 50; // Has to match clients

	public int localPort = 26000; // Has to match clients
	public int webPort = 26001; // Has to match clients

	public string IP = "192.168.1.134"; // Has to match clients

	[Header("[ Status ]")]
	public bool active;

	public int localHostID = -1;
	public int webHostID = -1;

	public byte errorCode;

	public NetworkError error = NetworkError.Timeout;

	[Header("[ Network Buffer ]")]
	public int bufferSize = 13; // Has to match clients

	[SerializeField]
	private byte[] _recBuffer = new byte[13];
	public byte[] recBuffer {
		get {
			if (_recBuffer == null) {
				_recBuffer = new byte[bufferSize];
			}

			return _recBuffer;
		}
		set {
			_recBuffer = value;
		}
	}

	public virtual void OnEnable()
	{
		Startup();
	}

	public virtual void OnDisable()
	{
		Shutdown();
	}

	public virtual void Startup()
	{
		NetworkTransport.Init();

		ConnectionConfig cc = new ConnectionConfig();

		cc.AddChannel(QosType.Unreliable);
		cc.AddChannel(QosType.Reliable);

		HostTopology topology = new HostTopology(cc, maximumConnections);

		// Server

		localHostID = NetworkTransport.AddHost(topology, localPort, IP);
		webHostID = NetworkTransport.AddWebsocketHost(topology, webPort, IP);

		recBuffer = new byte[bufferSize];

		active = true;

		Debug.LogFormat("Server started - LocalPort [{0}] WebPort [{1}]", localPort, webPort);
	}

	public virtual void Shutdown()
	{
		active = false;

		Debug.LogFormat("Server shut down.");

		NetworkTransport.Shutdown();
	}

	public virtual void Update()
	{
		if (!active) {
			return;
		}

		UpdateMessagePump();
		SendBuffer();
	}

	public virtual void UpdateMessagePump()
	{
		if (!active) {
			return;
		}

		int recHostId; // Web or standalone?
		int connectionID; // Which connection is this coming from?
		int channelID; // Which channel?

		int dataSize;

		NetworkEventType eventType = NetworkTransport.Receive(out recHostId, out connectionID, out channelID, recBuffer, bufferSize, out dataSize, out errorCode);

		error = (NetworkError)errorCode;

		switch (eventType) {
			case NetworkEventType.ConnectEvent:
				HandleConnectEvent(connectionID);
				break;
			case NetworkEventType.DisconnectEvent:
				HandleDisconnectEvent(connectionID);
				break;
			case NetworkEventType.DataEvent:
				ReceiveBuffer(connectionID);
				break;
			case NetworkEventType.BroadcastEvent:
				break;
		}
	}

	public virtual void HandleConnectEvent(int connectionID) {
		Debug.LogFormat("User connected [{0}]", connectionID);
	}

	public virtual void HandleDisconnectEvent(int connectionID) {
		Debug.LogFormat("User disconnected [{0}]", connectionID);
	}

	public virtual void ReceiveBuffer(int connectionID) {

	}

	public virtual void SendBuffer() {

	}
}