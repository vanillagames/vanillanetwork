﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Vanilla.Network;
using Vanilla.ExtensionMethods;

[Serializable]
public class NetworkPose {
	// Position as represented by a short-compressed Vector3
	[SerializeField]
	public short pX;
	[SerializeField]
	public short pY;
	[SerializeField]
	public short pZ;

	// Rotation as represented by smallest-three quaternion compression
	[SerializeField]
	public short rA;
	[SerializeField]
	public short rB;
	[SerializeField]
	public short rC;
	[SerializeField]
	public byte rMaxIndex;

}

//public class NetworkPoseSerializer : VanillaSerializer<NetworkPose> {
//	public override byte[] BytePackingSerialization(NetworkPose payload) {

//	}

//	public override NetworkPose BytePackingDeserialization(byte[] bytes) {

//	}
//}

public class OurPlanetClient : Client
{
	public Transform ARAnchor;

	ShortQuaternion r;

	//private NetworkPoseSerializer _networkPoseSerializer;
	//public NetworkPoseSerializer networkPoseSerializer {
	//	get {
	//		if (_networkPoseSerializer == null) {
	//			_networkPoseSerializer = new NetworkPoseSerializer();
	//		}

	//		return _networkPoseSerializer;
	//	}
	//}

	public override void ConnectToServerSucceeded() {
		base.ConnectToServerSucceeded();
	}

	public override void ConnectToServerFailed() {
		base.ConnectToServerFailed();
	}

	public override void DisconnectedFromServer() {
		base.DisconnectedFromServer();
	}

	public override void Update() {
		base.Update();
	}

	public override void UpdateMessagePump() {
		base.UpdateMessagePump();
	}

	public override void ReceiveBuffer(int connectionID) {
		base.ReceiveBuffer(connectionID);
	}

	public override void SendBuffer() {
		// This had to be reduced from the estimated 1000.0f to 100.0f. Where did the extra 10x factor go..?
		Vector3 p = ARAnchor.InverseTransformPoint(transform.position) * 1000.0f;

		BitConverter.GetBytes((short)p.x).CopyTo(sendBuffer, 0);
		BitConverter.GetBytes((short)p.y).CopyTo(sendBuffer, 2);
		BitConverter.GetBytes((short)p.z).CopyTo(sendBuffer, 4);

		//r = transform.rotation.ToShortQuaternion(1000.0f);
		r = Quaternion.LookRotation(ARAnchor.InverseTransformDirection(transform.forward)).ToShortQuaternion(1000.0f);

		BitConverter.GetBytes(r.rA).CopyTo(sendBuffer, 6);
		BitConverter.GetBytes(r.rB).CopyTo(sendBuffer, 8);
		BitConverter.GetBytes(r.rC).CopyTo(sendBuffer, 10);

		sendBuffer[12] = r.rMaxIndex;
		
		//sendBuffer = networkPoseSerializer.BytePackingSerialization(

		base.SendBuffer();
	}
}